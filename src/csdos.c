#include "csdos.h"

#include <ncurses.h>
#include <memory.h>
#include <ncurses.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 4096

typedef struct {
  char command[BUFFER_SIZE];
  char output[BUFFER_SIZE];
} command_history_item;

int running = 1;

char buffer[BUFFER_SIZE];
size_t current_char = 0;

command_history_item *command_history_buffer;
size_t current_command_index = 0;

char current_disk_label = 'C';

int ch;

void CSDOS_setup_ncurses_window(void) {
  initscr();
  start_color();
  noecho();
}

void CSDOS_setup_ncurses_keyboard(void) {
  cbreak();
  keypad(stdscr, TRUE);
  curs_set(TRUE);
  scrollok(stdscr, TRUE);
}

void CSDOS_setup_ncurses_colors(void) {
  init_pair(1, COLOR_WHITE, COLOR_BLACK);
  attron(COLOR_PAIR(1));
}

void CSDOS_setup_ncurses(void) {
  CSDOS_setup_ncurses_window();
  CSDOS_setup_ncurses_colors();
  CSDOS_setup_ncurses_keyboard();
}

void CSDOS_setup_variables(void) {
  memset(buffer, 0, BUFFER_SIZE);
  command_history_buffer = (command_history_item *)malloc(BUFFER_SIZE * BUFFER_SIZE);
}

void CSDOS_setup(void) {
  CSDOS_setup_ncurses();
  CSDOS_setup_variables();
}

void CSDOS_print_terminal_disk(void) {
  printw("%c:\\>", current_disk_label);
}

void CSDOS_print_command_history(void) {
    for (int i = 0; i < current_command_index; i++) {
      CSDOS_print_terminal_disk();
      printw("%s\n", command_history_buffer[i].command);
      printw("%s\n", command_history_buffer[i].output);
    }
}

void CSDOS_print_starting_message(void) {
    printw("STARTING CS-DOS...\n\n");
}

void CSDOS_print_terminal_input(void) {
  CSDOS_print_terminal_disk();
  printw("%s", buffer);
}

void CSDOS_print_terminal(void) {
  CSDOS_print_starting_message();
  CSDOS_print_command_history();
  CSDOS_print_terminal_input();
}

void CSDOS_handle_keypress(void) {
  /* temporary implementation, please ignore */
  ch = getch();

  if (ch >= 'a' && ch <= 'z') {
    char ascii = ch & 0x000000FF;
    buffer[current_char++] = ascii - 32;
  } else if (ch >= 32 && ch <= 126) {
    char ascii = ch & 0x000000FF;
    buffer[current_char++] = ascii;
  } else if (ch == KEY_BACKSPACE && current_char > 0) {
    buffer[--current_char] = 0;
  } else if (ch == 10) {
    command_history_item new_item;
    strncpy(new_item.command, buffer, BUFFER_SIZE);
    strcpy(new_item.output, "BAD COMMAND OR FILE NAME");
    command_history_buffer[current_command_index++] = new_item;

    memset(buffer, 0, BUFFER_SIZE);
    current_char = 0;
  }
}

void CSDOS_cleanup(void) {
  free(command_history_buffer);
  attroff(COLOR_PAIR(1));
  endwin();
}

void CSDOS_clear_screen(void) {
  clear();
  refresh();
}

void CSDOS_start_main_loop(void) {
  while(running) {
    CSDOS_print_terminal();
    CSDOS_handle_keypress();
    CSDOS_clear_screen();
  }
}

void CSDOS_startup(void) {
  CSDOS_setup();
  CSDOS_start_main_loop();
  CSDOS_cleanup();
}
