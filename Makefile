CC := gcc
CFLAGS := -Wall -o3  
LDFLAGS := 
LIBS := ncurses

SRC := src
OBJ := obj

SRCS := $(wildcard $(SRC)/*.c)
OBJS := $(patsubst $(SRC)/%.c, $(OBJ)/%.o, $(SRCS))

EXE := cs-dos


.PHONY:
	$(EXE)

$(EXE): $(OBJ) $(OBJS) 
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJS) -o $(EXE) `pkg-config --libs --cflags $(LIBS)` 

$(OBJ): 
	mkdir $@

$(OBJS): $(OBJ)/%.o : $(SRC)/%.c
	$(CC) $(CFLAGS) -c $< -o $@ `pkg-config --libs --cflags $(LIBS)` 

clean:
	rm -rf obj $(EXE)
