# cs-dos

A simple SubRosa's CS-DOS clone written in C using the NCurses library.

## Getting Started

```sh
make && ./cs-dos
```
